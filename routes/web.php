<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'SuperController@index')->name('home');
Route::get('/rp', 'SuperController@rp')->name('rp');
Route::post('/ticket/store', 'SuperController@storeTicket')->name('ticket.store');
