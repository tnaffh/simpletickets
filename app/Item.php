<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    function tickets(){
        return $this->belongsToMany(Ticket::class)->withPivot('quantity');
    }
}
