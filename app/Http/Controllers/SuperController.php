<?php

namespace App\Http\Controllers;

use App\Item;
use App\Ticket;
use App\User;
use Illuminate\Http\Request;

class SuperController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('home');
    }

    function storeTicket(Request $request)
    {
        $items = Item::all();
        if ($items->count() < 1) {
            abort(404, "No items found");
        }


        $ticket_items = collect();
        foreach ($items as $item) {
            if ($request->has($item->id) && (int)$request->input($item->id) > 0) {
                $_item = collect(['id' => $item->id, 'quantity' => $request->input($item->id)]);
                $ticket_items->push($_item);
            }
        }

        if ($ticket_items->count() < 0) {
            return redirect()->back()->with('warming', 'Ticket has no items!');
        }

        $ticket = new Ticket();
        //$user = auth()->user();

        $code = $this->generateRandomString(10);
        $ticket->code = $code;
        $ticket->user()->associate(auth()->user());
        $ticket->save();

        $ticket->code = str_pad($ticket->id, 10, 0, STR_PAD_LEFT);
        $ticket->save();

        foreach ($ticket_items as $item) {
            $ticket->items()->attach($item->get('id'), ['quantity' => $item->get("quantity")]);
        }


        return view('receipt',['ticket' => $ticket]);

        //return redirect()->back()->with('warming', 'Ticket generated successfully!');

    }

    function rp(){
        return view('receipt');
    }

    public function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
