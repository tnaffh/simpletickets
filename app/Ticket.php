<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    function items(){
        return $this->belongsToMany(Item::class)->withPivot('quantity');
    }

    function user(){
        return $this->belongsTo(User::class);
    }

    function amount(){
        $total =0;
        foreach ($this->items as $item){
            $total = ($total + ($item->pivot->quantity * $item->price));
        }

        return $total;
    }
}
