<!-- New baby modal -->
<div class="modal" id="modalNewBaby">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">New Ticket</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" id="form-new-baby" action="{{ route("ticket.store") }}">
                    @csrf

                    @foreach(\App\Item::all() as $item)
                        <div class="form-group row">
                            <label for="{{ $item->id }}"
                                   class="col-md-6 col-form-label text-md-right">{{ $item->name }}</label>

                            <div class="col-md-4">
                                <input id="first_name" type="number"
                                       class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}"
                                       name="{{ $item->id }}" value="0" required autofocus>

                                @if ($errors->has('first_name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    @endforeach

                    {{--<div class="form-group row">
                        <label for="parent" class="col-md-4 col-form-label text-md-right">{{ __('Parent') }}</label>
                        <div class="col-md-6" style="margin-top: 5px">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="parent_role" id="inlineRadio1" value="Father">
                                <label class="form-check-label" for="inlineRadio1"> Father</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="parent_role" id="inlineRadio2" value="Mother">
                                <label class="form-check-label" for="inlineRadio2"> Mother</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="parent_role" id="inlineRadio3" value="Guardian" checked>
                                <label class="form-check-label" for="inlineRadio3"> Guardian</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="parent_name" class="col-md-4 col-form-label text-md-right">{{ __('Parent name') }}</label>

                        <div class="col-md-6">
                            <input id="parent_name" type="text" class="form-control{{ $errors->has('parent_name') ? ' is-invalid' : '' }}" name="parent_name" value="{{ old('parent_name') }}" required>

                            @if ($errors->has('parent_name'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('parent_name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="mobile" class="col-md-4 col-form-label text-md-right">{{ __('Mobile Number') }}</label>

                        <div class="col-md-6">
                            <input id="mobile" type="text" class="form-control{{ $errors->has('mobile') ? ' is-invalid' : '' }}" name="mobile" value="{{ old('mobile') }}" required>

                            @if ($errors->has('mobile'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="language_id" class="col-md-4 col-form-label text-md-right">{{ __('Preferred language') }}</label>

                        <div class="col-md-6">
                            <select id="language_id" class="form-control{{ $errors->has('language_id') ? ' is-invalid' : '' }}" name="language_id" required>
                                <option value="">Please select</option>
                                @foreach(\App\Language::all() as $lang)
                                    <option value="{{ $lang->id }}">{{ $lang->name }}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('language_id'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('language_id') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>--}}

                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" form="form-new-baby" class="btn btn-primary">Save changes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>