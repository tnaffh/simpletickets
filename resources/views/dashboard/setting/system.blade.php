@extends('layouts.dashboard')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">System Setting</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <button class="btn btn-sm btn-outline-primary">New Baby</button>
                <button class="btn btn-sm btn-outline-primary">New System Setting</button>
            </div>
        </div>
    </div>

    <div class="row">

        <h4>Messages</h4>
        <div class="row">
            @foreach(\App\Config::where('type','message')->get() as $m)
                <div class="col-md-4">
                    <div class="card text-white bg-primary mb-3">
                        {{--<div class="card-header">Header</div>--}}
                        <div class="card-body">
                            <h4 class="card-title">{{ $m->name }}</h4>
                            <p class="card-text">{{ str_replace('{date}',\Carbon\Carbon::tomorrow(),str_replace('{dob}',\Carbon\Carbon::yesterday(),str_replace('{name}','Dona David',$m->value))) }}</p>
                        </div>
                        <dv class="card-footer">
                            <button class="btn btn-sm btn-outline-light">Edit</button>
                        </dv>
                    </div>
                </div>
            @endforeach
        </div>


    </div>
@endsection