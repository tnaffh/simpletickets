@extends('layouts.dashboard')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">- {{ $guardian->name }}</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <button class="btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#modalNewBaby">New
                    Baby
                </button>
                {{--<button class="btn btn-sm btn-outline-warning">Delete this baby</button>--}}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">

            <div class="card border-dark mb-3">
                {{--<div class="card-header">Vaccination schedule</div>--}}
                <div style="height: 140px" class="text-center bg-info p-3 mb-2 pb-4">
                    <img src="{{ asset('img/baby.png') }}" height="100%" alt="" class="pb-1">
                    <h5 class="text-center text-light">{{ $guardian->name }}</h5>
                </div>

                <h6 class="text-center">Guardians/Parents</h6>
                @foreach($guardian->babies as $baby)
                    <ul class="list-group">
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="{{ route('babies.show',['baby'=> $baby->id]) }}">{{ $baby->first_name }}</a>

                            <span class="badge badge-primary badge-pill">{{ $baby->pivot->role }}</span>
                        </li>
                    </ul>
                @endforeach
                {{--<div class="list-group">
                    <a href="" class="list-group-item list-group-item-action flex-column align-items-start active">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">Guardians</h5>
                            <small>{{ $baby->guardians->count() }}</small>
                        </div>

                    </a>
                    <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">List group item heading</h5>
                            <small class="text-muted">3 days ago</small>
                        </div>
                        <p class="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
                        <small class="text-muted">Donec id elit non mi porta.</small>
                    </a>
                    <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">List group item heading</h5>
                            <small class="text-muted">3 days ago</small>
                        </div>
                        <p class="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
                        <small class="text-muted">Donec id elit non mi porta.</small>
                    </a>
                </div>--}}
                <div class="card-body">

                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="card border-primary mb-3">
                <div class="card-header">Messages</div>
                <div class="table-responsive table-striped">
                    <table class="table table-bordered table-striped table-sm">
                        <thead>
                        <tr>
                            <th>Term</th>
                            <th>Estimated date</th>
                            <th>Vaccination for</th>
                            <th>Status</th>

                        </tr>
                        </thead>
                        <tbody>

                        @foreach($baby->schedule() as $s)
                            <tr class="table-{{ $s->vaccination_date < \Carbon\Carbon::today() ? 'danger' : 'info' }}">
                                <td>{{ $s->term }}</td>
                                <td>{{ \Carbon\Carbon::parse($s->vaccination_date)->formatLocalized('%a, %d %b %Y') }}</td>
                                <td>{{ $s->name }}</td>
                                <td> Pending</td>

                            </tr>

                        @endforeach

                        </tbody>
                    </table>
                </div>
                <div class="card-body">

                </div>
            </div>
        </div>

    </div>



@endsection