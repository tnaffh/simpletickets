@extends('layouts.dashboard')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Guardians / Parents</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <button class="btn btn-sm btn-outline-primary">Import exel file</button>
                {{--<button class="btn btn-sm btn-outline-primary">New User</button>--}}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

            <div class="card border-primary mb-3">
                <div class="card-header">Guardians / Parents</div>
                <div class="table-responsive table-striped">
                    <table class="table table-striped table-sm">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Role</th>
                            <th>Mobile</th>
                            <th>Actions</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach(\App\Guardian::all() as $guardian)
                            <tr>
                                <td>{{ $guardian->name }}</td>
                                <td>
                                    @foreach($guardian->babies as $baby)
                                        <span class="badge badge-{{ strtolower($baby->pivot->role) === strtolower('mother') ? 'success' : strtolower($baby->pivot->role) === strtolower('father') ? 'primary' : 'info' }}">{{ $baby->pivot->role }}</span>
                                    @endforeach
                                </td>
                                <td>{{ $guardian->mobile }}</td>
                                <td><button class="btn btn-sm btn-danger">Delete</button></td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-body">

                </div>
            </div>

        </div>
    </div>
@endsection