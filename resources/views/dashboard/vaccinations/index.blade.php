@extends('layouts.dashboard')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Vaccinations</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <button class="btn btn-sm btn-outline-primary">New Baby</button>
                <button class="btn btn-sm btn-outline-primary">New Vaccinations</button>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

            <div class="card border-primary mb-3">
                <div class="card-header">Vaccinations</div>
                <div class="table-responsive table-striped">
                    <table class="table table-striped table-sm">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Weeks</th>
                            <th>Term</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach(\App\Vaccination::all() as $v)
                            <tr>
                                <td>{{ $v->name}}</td>
                                <td>{{ $v->weeks}}</td>
                                <td>{{ $v->term }}</td>
                                <td><button class="btn btn-sm btn-danger">Delete</button></td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-body">

                </div>
            </div>

        </div>
    </div>
@endsection