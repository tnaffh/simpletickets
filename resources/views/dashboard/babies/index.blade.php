@extends('layouts.dashboard')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Babies</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <button class="btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#modalNewBaby">New
                    Baby
                </button>
                <button class="btn btn-sm btn-outline-primary">New User</button>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

            <div class="card border-primary mb-3">
                <div class="card-header">Current babies</div>
                <div class="table-responsive table-striped">
                    <table class="table table-striped table-sm">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Date of birth</th>
                            <th>Guardians</th>
                            <th>Mobile</th>
                            <th>Role</th>
                            <th>Quick action</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach(\App\Baby::all() as $baby)
                            <tr>
                                <td>{{ $baby->first_name.' '.$baby->last_name }}</td>
                                <td>{{ $baby->date_of_birth }}</td>
                                <td>
                                    @foreach($baby->guardians as $guardian)
                                        <span class="badge badge-{{ strtolower($guardian->pivot->role) === strtolower('mother') ? 'success' : strtolower($guardian->pivot->role) === strtolower('father') ? 'primary' : 'info' }}">{{ $guardian->name }}</span>
                                    @endforeach
                                </td>
                                <td>
                                    @foreach($baby->guardians as $guardian)
                                        {{ $guardian->mobile }}
                                    @endforeach
                                </td>
                                <td>
                                    @foreach($baby->guardians as $guardian)
                                        <span class="badge badge-{{ strtolower($guardian->pivot->role) === strtolower('mother') ? 'success' : strtolower($guardian->pivot->role) === strtolower('father') ? 'primary' : 'info' }}">{{ $guardian->pivot->role }}</span>
                                    @endforeach
                                </td>
                                <td>
                                    @foreach($baby->guardians as $guardian)
                                        <span class="badge badge-secondary">{{ $guardian->language->name }}</span>
                                    @endforeach
                                </td>
                                <td>
                                    <a href="{{ route('babies.show',['baby' =>$baby->id]) }}" class="btn btn-sm btn-outline-info">Open</a>
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-body">

                </div>
            </div>

        </div>
    </div>


@endsection