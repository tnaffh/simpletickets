@extends('layouts.dashboard')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"></h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <button class="btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#modalNewBaby">New
                    Baby
                </button>
                {{--<button class="btn btn-sm btn-outline-primary">New User</button>--}}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-8 col-md-12 col-sm-12">

            <div class="card border-primary mb-3">
                <div class="card-header">Baby registration</div>

                <div class="card-body">
                    <form method="POST" id="form-new-baby-page" action="{{ route('babies.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="first_name"
                                   class="col-md-4 col-form-label text-md-right">{{ __('First Name') }}</label>

                            <div class="col-md-8">
                                <input id="first_name" placeholder="First name" type="text"
                                       class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}"
                                       name="first_name" value="{{ old('first_name') }}" required autofocus>

                                @if ($errors->has('first_name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="last_name"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Last Name') }}</label>

                            <div class="col-md-8">
                                <input id="last_name" placeholder="Last name" type="text"
                                       class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}"
                                       name="last_name" value="{{ old('last_name') }}" requirautofocus>

                                @if ($errors->has('last_name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="date_of_birth"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Date of birth') }}</label>

                            <div class="col-md-8">
                                <input id="date_of_birth" type="date"
                                       class="form-control{{ $errors->has('date_of_birth') ? ' is-invalid' : '' }}"
                                       name="date_of_birth" value="{{ old('date_of_birth') }}" required autofocus>

                                @if ($errors->has('date_of_birth'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('date_of_birth') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- Parents fields -->
                        <ul class="nav nav-tabs justify-content-center">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#mother">Mother</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#father">Father</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#guardian">Guardian</a>
                            </li>

                            {{--<li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Dropdown</a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Separated link</a>
                                </div>
                            </li>--}}
                        </ul>
                        <div id="myTabContent" class="tab-content">
                            <div class="tab-pane fade show active" id="mother">
                                <br>
                                <div class="form-group row">
                                    <label for="mother_first_name"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Mother\'s name') }}</label>

                                    <div class="col-md-4">
                                        <input id="mother_first_name" type="text" placeholder="First name"
                                               class="form-control{{ $errors->has('mother_first_name') ? ' is-invalid' : '' }}"
                                               name="mother_first_name" value="{{ old('mother_first_name') }}">

                                        @if ($errors->has('mother_first_name'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('mother_first_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                    <div class="col-md-4">
                                        <input id="mother_last_name" type="text" placeholder="Last name"
                                               class="form-control{{ $errors->has('mother_last_name') ? ' is-invalid' : '' }}"
                                               name="mother_last_name" value="{{ old('mother_last_name') }}">

                                        @if ($errors->has('mother_last_name'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('mother_last_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="mother_mobile"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Mother\'s Mobile Number') }}</label>

                                    <div class="col-md-8">
                                        <input id="mother_mobile" type="tel" placeholder="0812345678"
                                               class="form-control{{ $errors->has('mother_mobile') ? ' is-invalid' : '' }}"
                                               name="mother_mobile" value="{{ old('mother_mobile') }}">

                                        @if ($errors->has('mother_mobile'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('mother_mobile') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="mother_language"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Mother\'s language') }}</label>

                                    <div class="col-md-8">
                                        <select id="mother_language"
                                                class="form-control{{ $errors->has('mother_language') ? ' is-invalid' : '' }}"
                                                name="mother_language">
                                            <option value="">Please select</option>
                                            @foreach(\App\Language::all() as $lang)
                                                <option value="{{ $lang->id }}">{{ $lang->name }}</option>
                                            @endforeach
                                        </select>

                                        @if ($errors->has('mother_language'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('mother_language') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="father">
                                <br>
                                <div class="form-group row">
                                    <label for="father_first_name"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Father\'s name') }}</label>

                                    <div class="col-md-4">
                                        <input id="father_first_name" type="text" placeholder="First name"
                                               class="form-control{{ $errors->has('father_first_name') ? ' is-invalid' : '' }}"
                                               name="father_first_name" value="{{ old('father_first_name') }}">

                                        @if ($errors->has('father_first_name'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('father_first_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                    <div class="col-md-4">
                                        <input id="father_last_name" type="text" placeholder="Last name"
                                               class="form-control{{ $errors->has('father_last_name') ? ' is-invalid' : '' }}"
                                               name="father_last_name" value="{{ old('father_last_name') }}">

                                        @if ($errors->has('father_last_name'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('father_last_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="father_mobile"
                                           class="col-md-4 col-form-label text-md-right">{{ __('father\'s Mobile Number') }}</label>

                                    <div class="col-md-8">
                                        <input id="father_mobile" type="text" placeholder="0812345678"
                                               class="form-control{{ $errors->has('father_mobile') ? ' is-invalid' : '' }}"
                                               name="father_mobile" value="{{ old('father_mobile') }}">

                                        @if ($errors->has('father_mobile'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('father_mobile') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="father_language"
                                           class="col-md-4 col-form-label text-md-right">{{ __('father\'s language') }}</label>

                                    <div class="col-md-8">
                                        <select id="father_language"
                                                class="form-control{{ $errors->has('father_language') ? ' is-invalid' : '' }}"
                                                name="father_language">
                                            <option value="">Please select</option>
                                            @foreach(\App\Language::all() as $lang)
                                                <option value="{{ $lang->id }}">{{ $lang->name }}</option>
                                            @endforeach
                                        </select>

                                        @if ($errors->has('father_language'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('father_language') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="guardian">
                                <br>
                                <div class="form-group row">
                                    <label for="guardian_first_name"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Guardian\'s name') }}</label>

                                    <div class="col-md-4">
                                        <input id="guardian_first_name" type="text" placeholder="First name"
                                               class="form-control{{ $errors->has('guardian_first_name') ? ' is-invalid' : '' }}"
                                               name="guardian_first_name" value="{{ old('guardian_first_name') }}">

                                        @if ($errors->has('guardian_first_name'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('guardian_first_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                    <div class="col-md-4">
                                        <input id="guardian_last_name" type="text" placeholder="Last name"
                                               class="form-control{{ $errors->has('guardian_last_name') ? ' is-invalid' : '' }}"
                                               name="guardian_last_name" value="{{ old('guardian_last_name') }}">

                                        @if ($errors->has('guardian_last_name'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('guardian_last_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="guardian_mobile"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Guardian\'s Mobile Number') }}</label>

                                    <div class="col-md-8">
                                        <input id="guardian_mobile" type="text"
                                               class="form-control{{ $errors->has('guardian_mobile') ? ' is-invalid' : '' }}"
                                               name="guardian_mobile" value="{{ old('guardian_mobile') }}"
                                               placeholder="0812345678">

                                        @if ($errors->has('guardian_mobile'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('guardian_mobile') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="guardian_language"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Guardian\'s language') }}</label>

                                    <div class="col-md-8">
                                        <select id="guardian_language"
                                                class="form-control{{ $errors->has('guardian_language') ? ' is-invalid' : '' }}"
                                                name="guardian_language">
                                            <option value="">Please select</option>
                                            @foreach(\App\Language::all() as $lang)
                                                <option value="{{ $lang->id }}">{{ $lang->name }}</option>
                                            @endforeach
                                        </select>

                                        @if ($errors->has('guardian_language'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('guardian_language') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- Parents fields /-->


                    </form>
                </div>
                <div class="card-footer text-right">
                    <button type="submit" form="form-new-baby-page" class="btn btn-success">Add a baby</button>
                </div>
            </div>

        </div>
    </div>


@endsection