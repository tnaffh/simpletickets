@extends('layouts.dashboard')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Users</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <button class="btn btn-sm btn-outline-primary">New Baby</button>
                <button class="btn btn-sm btn-outline-primary">New User</button>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

            <div class="card border-primary mb-3">
                <div class="card-header">Current users</div>
                <div class="table-responsive table-striped">
                    <table class="table table-striped table-sm">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>Actions</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach(\App\User::all() as $user)
                            <tr>
                                <td>{{ $user->first_name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->mobile }}</td>
                                <td><button class="btn btn-sm btn-danger">Delete</button></td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-body">

                </div>
            </div>

        </div>
    </div>
@endsection