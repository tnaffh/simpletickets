@extends('layouts.dashboard')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Dashboard</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <button class="btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#modalNewBaby">New Ticket
                </button>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 text-center">
            <div class="card text-white bg-primary mb-3">
                {{--<div class="card-header">Babies</div>--}}
                <div class="card-body">
                    <h1 class="card-title">{{ \App\Ticket::count() }}</h1>
                    <p class="card-text">Total Tickets</p>
                </div>
            </div>
        </div>
        <div class="col-md-3 text-center">
            <div class="card text-white bg-success mb-3">
                {{--<div class="card-header">Users</div>--}}
                <div class="card-body">
                    <h1 class="card-title">{{ \App\Core::totalAmount() }}</h1>
                    <p class="card-text">Revenue</p>
                </div>
            </div>
        </div>
        <div class="col-md-3 text-center">
            <div class="card text-white bg-warning mb-3">
                {{--<div class="card-header">Messages</div>--}}
                <div class="card-body">
                    <h1 class="card-title">{{ \App\Core::totalKids() }}</h1>
                    <p class="card-text">Kids</p>
                </div>
            </div>
        </div>
        <div class="col-md-3 text-center">
            <div class="card text-white bg-danger mb-3">
                {{--<div class="card-header">Messages</div>--}}
                <div class="card-body">
                    {{-- Carbon::parse($baby->nextVaccinationDate()) === Carbon::today()->addDay(1) --}}

                    <h1 class="card-title">{{ \App\User::count()  }}</h1>
                    <p class="card-text">Babies Due today</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">

            <div class="card bg-light  mb-3">
                <div class="card-header">Tickets</div>
                <div class="table-responsive table-striped">
                    <table class="table table-striped table-sm">
                        <thead>
                        <tr>
                            <th>Code</th>
                            <th>Time</th>
                            <th>Amount</th>
                            <th>Agent</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach(\App\Ticket::latest()->get() as $t)
                            <tr>
                                <td><span class="badge badge-primary">{{ $t->code }}</span></td>
                                <td>{{ $t->created_at }}</td>
                                <td><span class="badge badge-info">N$ {{ $t->amount() }}</span></td>
                                <td><span class="badge badge-success">{{ $t->user->name }}</span></td>
                            {{--@if($sms->status === 'success')
                                    <td><span class="badge badge-success">{{ $sms->amount }}</span></td>
                                @endif
                                @if($sms->status === 'failed')
                                    <td><span class="badge badge-danger">{{ $sms->status }}</span></td>
                                @endif
                                @if($sms->status === 'queued')
                                    <td><span class="badge badge-warning">{{ $sms->status }}</span></td>
                                @endif--}}


                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{--<div class="card-body">

                </div>--}}
            </div>

        </div>
        {{--<div class="col-md-3">

            <div class="card bg-light  mb-3">
                <div class="card-header">Latest 25 babies</div>
                <div class="table-responsive table-striped">
                    <table class="table table-striped table-sm">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Due in</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach(\App\Baby::latest()->take(25)->get() as $baby)
                            <tr>
                                <td>{{ $baby->first_name.' '.$baby->last_name }}</td>
                                <td>
                                    <span class="badge badge-primary">{{ \Carbon\Carbon::parse($baby->nextVaccinationDate())->diffForHumans() }}</span>
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-body">

                </div>
            </div>

        </div>--}}
    </div>


@endsection
