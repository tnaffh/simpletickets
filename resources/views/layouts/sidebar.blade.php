<div class="sidebar-sticky">
    <ul class="nav flex-column">
        <li class="nav-item">
            <a class="nav-link active" href="{{ url('/') }}">
                <span data-feather="hash"></span>
                Dashboard <span class="sr-only">(current)</span>
            </a>
        </li>


    </ul>
    <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
        <span>Tickets</span>
        <a class="d-flex align-items-center text-muted" href="#">
            <span data-feather="user"></span>
        </a>
    </h6>
    <ul class="nav flex-column mb-2">
        <li class="nav-item">
            <a class="nav-link" href="{{ route('home') }}">
                <span data-feather="users"></span>
                Tickets
            </a>
        </li>
        {{--<li class="nav-item">
            <a class="nav-link" href="{{ route('guardians.index') }}">
                <span data-feather="users"></span>
                Parents / guardians
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('babies.index') }}">
                <span data-feather="sun"></span>
                All Babies
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('babies.create') }}">
                <span data-feather="sun"></span>
                Add a baby
            </a>
        </li>--}}
    </ul>

    <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
        <span>Reports</span>
        <a class="d-flex align-items-center text-muted" href="#">
            <span data-feather="heart"></span>
        </a>
    </h6>
    <ul class="nav flex-column mb-2">
        <li class="nav-item">
            <a class="nav-link" href="{{ route('home') }}">
                <span data-feather="activity"></span>
                Tickets
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <span data-feather="plus-square"></span>
                Revenues
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Attendance
            </a>
        </li>


    </ul> <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
        <span>Administration</span>
        <a class="d-flex align-items-center text-muted" href="#">
            <span data-feather="message-circle"></span>
        </a>
    </h6>
    <ul class="nav flex-column mb-2">
        <li class="nav-item">
            <a class="nav-link" href="#">
                <span data-feather="send"></span>
                Users
            </a>
        </li>
        {{--<li class="nav-item">
            <a class="nav-link" href="#">
                <span data-feather="list"></span>
                SMS Monitor
            </a>
        </li>--}}
    </ul>

    <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
        <span>Settings</span>
        <a class="d-flex align-items-center text-muted" href="#">
            <span data-feather="sliders"></span>
        </a>
    </h6>
    <ul class="nav flex-column mb-2">
       {{-- <li class="nav-item">
            <a class="nav-link" href="#">
                <span data-feather="user"></span>
                Account settings
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('dashboard.system.setting') }}">
                <span data-feather="settings"></span>
                System Settings
            </a>
        </li>--}}
        <li class="nav-item">

            <a class="nav-link" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                <span data-feather="log-out"></span>
                {{ __('Logout') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>

    </ul>
</div>