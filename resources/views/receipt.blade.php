<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Ticket#: {{ $ticket->code }} Sales Receipt</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>

<div class="container-fluid">
    <div class="row">
        <div class="">
            <div class="order-details align-content-center">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xl-12">
                        <h2 class="text-center">Olufuko Anual Cultural Festival 2018</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xl-12">
                        <p class="text-center">26 August - 05 September 2018</p>
                    </div>
                </div>
                <div class="row">
                    <div class="ccol-md-12 col-lg-12 col-sm-12 col-xl-12">
                        <h5 class="text-center">Olufuko Festival Centre, Outapi</h5>
                    </div>
                </div>
                <div class="col-md-12 col-lg-12 col-sm-12 col-xl-12">
                    <h3 class="text-center">Sale receipt</h3>
                </div>
                <div class="col-md-12 col-lg-12 col-sm-12 col-xl-12">
                    <h3 class="text-center">{{ \Carbon\Carbon::parse($ticket->created_at)->toDayDateTimeString() }}</h3>
                </div>
            </div>

            <div class="row">

                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th class="col-md-6">Description</th>
                        <th class="col-md-1 text-right">Qnty</th>
                        <th class="col-md-1 text-right">Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($ticket->items as $item)
                        <tr>
                            <td class="">{{ $item->name }}</td>
                            <td class="text-right">{{ $item->pivot->quantity }}</td>
                            <td class="text-right">N${{ $item->price * $item->pivot->quantity }}</td>
                        </tr>
                    @endforeach


                    <tr>

                        <td class=""><strong>Sub Total</strong></td>
                        <td></td>
                        <td class="text-right">
                            <strong>N${{ $ticket->amount() }}</strong>
                        </td>
                    </tr>


                    </tbody>
                </table>
                <div class="col-md-12 col-lg-12 col-sm-12 col-xl-12 text-center">
                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPYAAAAeAQMAAAAly3FkAAAABlBMVEX///8AAABVwtN+AAAAAXRSTlMAQObYZgAAADJJREFUKJFj+Mx/+PCHM/w89jZ/zn/+zG8PZBj8+XD4w+fzn88zf7ZhGJUflR+VH7TyAKwXGPUv3Cu8AAAAAElFTkSuQmCC">
                    <br>
                    <h3 style="margin:5px 0">{{ $ticket->code }}</h3>
                    <br>
                    <h3 style="margin:5px 0">Thank you for your support</h3>
                    <!--
                        display: inline-block;
                    margin-top: -20px;
                    background: #FFF;
                    position: relative;
                    width: auto;
                    /* float: left; */
                    top: -18px;
                    padding: 0px 5px;
                    -->
                </div>
                <p class="text-center"></p>
                {{--<p class="text-center">Ticket #: {{ $ticket->code }}</p>--}}
                <div class="container-fluid hideOnPrint">
                    <div class="row hideOnPrint">
                        <div class="col-lg-12">
                            <a href="{{ route('home') }}"
                               class="btn btn-success btn-lg btn-block">Return to the Orders list</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    * {
        text-transform: uppercase;
    }

    @media print {
        * {
            font-family: Verdana, Geneva, sans-serif;
            text-transform: uppercase;
        }

        .hideOnPrint {
            display: none !important;
        }

        td, th {
            font-size: 2.8vw;
        }

        .order-details, p {
            font-size: 2.5vw;
        }

        .order-details h2 {
            font-size: 5.5vw;
        }

        h3 {
            font-size: 2.8vw;
        }

        h4 {
            font-size: 2.8vw;
        }
    }

    /* @page{
        margin-left: 5px;
        margin-right: 10px;
    } */
</style>
<script>
    window.print();
    window.close();
</script>
</body>
</html>

